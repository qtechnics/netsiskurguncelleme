# NetsisKurGencelleme - NetOpenX ile Netsis Otomatik Kur Güncelleme Servisi 

## Başlarken

Netsis NetOpenX mimarisi kullanarak günlük yada belirli bir tarihten günümüze döviz kurlarının Türkiye Cumhuriyet Merkez Bankası'ndan çekilip sisteme kaydedilmesi için oluşturulmuş bir servistir.

### Gereksinimler

* Netsis
* NetOpenX Lisansı

### Kurulum

NetsisKurGuncelle.exe.config dosyasındaki parametreler düzenlenerek görev zamanlayıcısı ile çalıştırılır. Geçmişe dönük kur güncellemesi yapılacak ise config dosyasında Gecmisten parametresi 1 yapılıp kurların çekileceği başlangıç tarihi gg.aa.yyyy şeklinde verilir. 

Proje kaynak kodlarından tekrar derlenebilir yada Downloads bölümündeki [Release.zip](https://bitbucket.org/qtechnics/netsiskurguncelleme/downloads/Release.zip) indirilerek derlenmiş hali kullanılabilir.

## Yazarlar

* **Muzaffer Ali AKYIL** - *Fikir sahibi* - [Victorious](https://muzaffer.akyil.net)

## Lisans

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details