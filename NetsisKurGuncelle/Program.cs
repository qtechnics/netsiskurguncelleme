﻿using NetOpenX50;
using Simplify.Log;
using System;

namespace NetsisKurGuncelle
{
    class Program
    {
        static void Main(string[] args)
        {
            TVTTipi vtTipi;
            switch (int.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("vtTipi")))
            {
                case 0:
                    vtTipi = TVTTipi.vtMSSQL;
                    break;
                case 1:
                    vtTipi = TVTTipi.vtReserved;
                    break;
                case 2:
                    vtTipi = TVTTipi.vtOracle;
                    break;
                case 3:
                    vtTipi = TVTTipi.vtAS400;
                    break;
                default:
                    vtTipi = TVTTipi.vtReserved;
                    break;
            }
            string vtAdi = System.Configuration.ConfigurationManager.AppSettings.Get("vtAdi");
            string vtKulAdi = System.Configuration.ConfigurationManager.AppSettings.Get("vtKulAdi");
            string vtKulSifre = System.Configuration.ConfigurationManager.AppSettings.Get("vtKulSifre");
            string netKul = System.Configuration.ConfigurationManager.AppSettings.Get("NetKul");
            string netSifre = System.Configuration.ConfigurationManager.AppSettings.Get("NetSifre");
            int subeKodu = int.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("SubeKodu"));
            int gecmissten = int.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("Gecmisten"));
            DateTime baslangicTarihi = DateTime.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("BaslangicTarihi"));
            try
            {

                Kernel kernel = new Kernel();
                Sirket sirketSQL = kernel.yeniSirket((TVTTipi)vtTipi, vtAdi, vtKulAdi, vtKulSifre, netKul, netSifre, subeKodu);
                Doviz doviz = kernel.yeniDoviz(sirketSQL);

                if (gecmissten == 1)
                {
                    // verilen tarihten itibaren döngülü
                    while (baslangicTarihi.Date != DateTime.Today.AddDays(1))
                    {
                        doviz.DovizKurGuncelle(baslangicTarihi.Date);
                        Console.WriteLine(baslangicTarihi.Date);
                        baslangicTarihi = baslangicTarihi.Date.AddDays(1);
                    }
                }
                else
                {
                    // Günlük kullanımlı hali
                    doviz.DovizKurGuncelle(DateTime.Today.Date);
                }
                doviz = null;
                sirketSQL = null;
                kernel = null;
                Logger.Default.Write("Kurlar Güncellendi.");
            }
            catch (Exception ex)
            {
                Logger.Default.Write(string.Format("Döviz güncelleme işleminde hata: {0}", ex.Message));
            }

        }
    }
}
